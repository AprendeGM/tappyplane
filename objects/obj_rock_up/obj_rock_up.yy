{
    "id": "7899aed8-e2e8-4316-80c9-fd9fbabcd744",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rock_up",
    "eventList": [
        {
            "id": "2ada29b9-0476-4b57-b066-1b50ec3a918b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7899aed8-e2e8-4316-80c9-fd9fbabcd744"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "5309bafc-d478-42e2-8dd0-82e71d7e3382",
    "visible": true
}