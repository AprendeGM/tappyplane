/// @DnDAction : YoYo Games.Random.Get_Random_Number
/// @DnDVersion : 1
/// @DnDHash : 09E89A6C
/// @DnDArgument : "var" "random_var"
/// @DnDArgument : "var_temp" "1"
/// @DnDArgument : "type" "1"
var random_var = floor(random_range(0, 1 + 1));

/// @DnDAction : YoYo Games.Common.If_Variable
/// @DnDVersion : 1
/// @DnDHash : 4FE913C7
/// @DnDArgument : "var" "random_var"
if(random_var == 0)
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 75F8CE7C
	/// @DnDParent : 4FE913C7
	/// @DnDArgument : "xpos" "850"
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_rock_down"
	/// @DnDSaveInfo : "objectid" "5907cd7b-6331-4290-8dc5-34bad5f8f00a"
	instance_create_layer(x + 850, y + 0, "Instances", obj_rock_down);
}

/// @DnDAction : YoYo Games.Common.Else
/// @DnDVersion : 1
/// @DnDHash : 11111834
else
{
	/// @DnDAction : YoYo Games.Instances.Create_Instance
	/// @DnDVersion : 1
	/// @DnDHash : 61F5E4D3
	/// @DnDParent : 11111834
	/// @DnDArgument : "xpos" "850"
	/// @DnDArgument : "xpos_relative" "1"
	/// @DnDArgument : "ypos" "480"
	/// @DnDArgument : "ypos_relative" "1"
	/// @DnDArgument : "objectid" "obj_rock_up"
	/// @DnDSaveInfo : "objectid" "7899aed8-e2e8-4316-80c9-fd9fbabcd744"
	instance_create_layer(x + 850, y + 480, "Instances", obj_rock_up);
}

/// @DnDAction : YoYo Games.Instances.Set_Alarm
/// @DnDVersion : 1
/// @DnDHash : 3F567E01
/// @DnDArgument : "steps" "60"
alarm_set(0, 60);