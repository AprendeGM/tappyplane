{
    "id": "8a59f256-e219-4d88-821f-02a9bb4e1817",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_over",
    "eventList": [
        {
            "id": "8d905b38-0a83-42be-bce4-dd6b66bc23e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 27,
            "eventtype": 10,
            "m_owner": "8a59f256-e219-4d88-821f-02a9bb4e1817"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e58b2508-db4c-4b2a-bfae-1438f572370b",
    "visible": true
}