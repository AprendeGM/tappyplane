{
    "id": "5907cd7b-6331-4290-8dc5-34bad5f8f00a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_rock_down",
    "eventList": [
        {
            "id": "133f1d98-c0ee-4264-9c3f-123675347d02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5907cd7b-6331-4290-8dc5-34bad5f8f00a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "5309bafc-d478-42e2-8dd0-82e71d7e3382",
    "visible": true
}