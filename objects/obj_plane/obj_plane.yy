{
    "id": "60c754b2-2c7f-4790-a983-926077e394c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_plane",
    "eventList": [
        {
            "id": "0f5a07df-58b3-429b-ac3f-27adf69ab09e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "60c754b2-2c7f-4790-a983-926077e394c9"
        },
        {
            "id": "7761ed05-cb44-4553-800a-cc69cbb216e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 10,
            "m_owner": "60c754b2-2c7f-4790-a983-926077e394c9"
        },
        {
            "id": "8929121b-bb01-4b20-9e98-82080a234a68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "5907cd7b-6331-4290-8dc5-34bad5f8f00a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "60c754b2-2c7f-4790-a983-926077e394c9"
        },
        {
            "id": "0e0fe6c9-08d0-4df5-bb65-49e92224f030",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7899aed8-e2e8-4316-80c9-fd9fbabcd744",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "60c754b2-2c7f-4790-a983-926077e394c9"
        },
        {
            "id": "d0af8c3c-d5af-4bdb-befd-fbf6ed06287c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "60c754b2-2c7f-4790-a983-926077e394c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0709cf53-d746-416d-a794-06d63235e777",
    "visible": true
}