{
    "id": "e58b2508-db4c-4b2a-bfae-1438f572370b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_game_over",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 77,
    "bbox_left": 0,
    "bbox_right": 411,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "6d63de71-6ef3-4222-b281-a73c14958f6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e58b2508-db4c-4b2a-bfae-1438f572370b",
            "compositeImage": {
                "id": "527aad48-5e51-4130-828b-eaf34d0b9d04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d63de71-6ef3-4222-b281-a73c14958f6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1ed2573-d5f3-4c58-9a75-4b96587927fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d63de71-6ef3-4222-b281-a73c14958f6e",
                    "LayerId": "27fff06e-abc0-472e-bcb8-42cb20eb8ef8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 78,
    "layers": [
        {
            "id": "27fff06e-abc0-472e-bcb8-42cb20eb8ef8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e58b2508-db4c-4b2a-bfae-1438f572370b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 412,
    "xorig": 206,
    "yorig": 39
}