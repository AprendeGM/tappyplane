{
    "id": "0709cf53-d746-416d-a794-06d63235e777",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_plane",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 72,
    "bbox_left": 0,
    "bbox_right": 87,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "475b930f-a384-4db0-ac52-86f908591d5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0709cf53-d746-416d-a794-06d63235e777",
            "compositeImage": {
                "id": "d5a01b24-2a5d-40bb-b68d-91245e85483b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "475b930f-a384-4db0-ac52-86f908591d5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78588aa2-78bb-480c-8be9-6ca78580b3ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "475b930f-a384-4db0-ac52-86f908591d5c",
                    "LayerId": "c24334b7-6755-4c9a-bba9-e829784ebeb8"
                }
            ]
        },
        {
            "id": "df22b166-d598-4bfb-82a0-ad34952bbc6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0709cf53-d746-416d-a794-06d63235e777",
            "compositeImage": {
                "id": "309a8142-0f29-4e6e-b143-953d34424b3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df22b166-d598-4bfb-82a0-ad34952bbc6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2de2d00d-78a3-467e-872b-cb90fd8970e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df22b166-d598-4bfb-82a0-ad34952bbc6c",
                    "LayerId": "c24334b7-6755-4c9a-bba9-e829784ebeb8"
                }
            ]
        },
        {
            "id": "f9432eeb-6be9-42b5-9d5e-8decb142dbc4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0709cf53-d746-416d-a794-06d63235e777",
            "compositeImage": {
                "id": "d41afd39-8950-49b5-8372-c90c12036b3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9432eeb-6be9-42b5-9d5e-8decb142dbc4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25664ae4-11e3-4b41-bcb9-f594950f8dc4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9432eeb-6be9-42b5-9d5e-8decb142dbc4",
                    "LayerId": "c24334b7-6755-4c9a-bba9-e829784ebeb8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 73,
    "layers": [
        {
            "id": "c24334b7-6755-4c9a-bba9-e829784ebeb8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0709cf53-d746-416d-a794-06d63235e777",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 88,
    "xorig": 0,
    "yorig": 0
}