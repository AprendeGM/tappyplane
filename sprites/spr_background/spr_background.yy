{
    "id": "37b011c3-489c-4cdb-9e79-a57e457de132",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "3940a477-69e8-4136-94d6-7bb5771110d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37b011c3-489c-4cdb-9e79-a57e457de132",
            "compositeImage": {
                "id": "955136cf-704a-48c3-a3b9-e495c969298e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3940a477-69e8-4136-94d6-7bb5771110d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f17b9880-e3b1-48f0-8a41-e48412a33ced",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3940a477-69e8-4136-94d6-7bb5771110d1",
                    "LayerId": "0e5894f4-f358-4d4c-b749-4e4f1b7896b3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 480,
    "layers": [
        {
            "id": "0e5894f4-f358-4d4c-b749-4e4f1b7896b3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37b011c3-489c-4cdb-9e79-a57e457de132",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}