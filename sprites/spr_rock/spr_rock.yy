{
    "id": "5309bafc-d478-42e2-8dd0-82e71d7e3382",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 238,
    "bbox_left": 0,
    "bbox_right": 107,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "frames": [
        {
            "id": "19b4d36e-d8e7-47eb-9ebe-0ca1b79b77e2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5309bafc-d478-42e2-8dd0-82e71d7e3382",
            "compositeImage": {
                "id": "f260c703-d353-467f-abed-7207e2414499",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19b4d36e-d8e7-47eb-9ebe-0ca1b79b77e2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "35424ec9-443a-4a0b-bdf6-fb801640d554",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19b4d36e-d8e7-47eb-9ebe-0ca1b79b77e2",
                    "LayerId": "5558445b-6a88-446e-b5da-aaffe53da630"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 239,
    "layers": [
        {
            "id": "5558445b-6a88-446e-b5da-aaffe53da630",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5309bafc-d478-42e2-8dd0-82e71d7e3382",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 108,
    "xorig": 0,
    "yorig": 0
}